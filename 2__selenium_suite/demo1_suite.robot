*** Settings ***
Documentation   This suite file handles facebook login activity
Library     SeleniumLibrary

*** Test Cases ***
TC1 FB Login
    Open Browser    url=https://www.facebook.com/       browser=chrome
    #get title and print it
    ${current_title}    Get Title
    Log To Console    ${current_title}

TC2
    Open Browser    url=https://www.facebook.com/       browser=edge
    Input Text    id=email    bala1234566767@gmail.com
    Input Password    id=pass    welcome1223
    #click on login
    Click Element      name=login
    Close Browser

TC3
    Open Browser    url=https://www.facebook.com/       browser=headlesschrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    #click on create new account
    Click Element    link=Create new account
    #enter firstname as john
    #checks the presence of locator in 0.5s
    Input Text    name=firstname    john
    Input Text    locator=name=lastname    text=wick
    Input Password    name=reg_passwd__    welcome123
    # 20 Dec 2000
    Select From List By Label    id=day     20
    Select From List By Label    id=month   Dec
    Select From List By Label    id=year    2000
    #select year as 2000
    #click on radio button Custom
#    //label[text()='Custom']
#    Click Element    locator=xpath=//input[@value='-1']
#    Select Radio Button    group_name=sex    value=-1
    Select Radio Button    sex    -1
    #click on sign up
    Capture Page Screenshot


