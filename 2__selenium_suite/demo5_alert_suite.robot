*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://netbanking.hdfcbank.com/netbanking/IpinResetUsingOTP.htm
    #click on GO
    Click Element    xpath=//img[@alt='Go']
    ${actual_alert_text}   Handle Alert    action=ACCEPT    timeout=20s
    Log    ${actual_alert_text}
    Should Be Equal    ${actual_alert_text}     Customer ID${SPACE}${SPACE}cannot be left blank.
    Should Be Equal As Strings    first=${actual_alert_text}    second=Customer ID${SPACE} cannot be left blank.
#    Alert Should Be Present


