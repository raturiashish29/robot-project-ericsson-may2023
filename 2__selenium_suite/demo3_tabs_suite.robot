*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1 Switch Tab to Title
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.db4free.net/
    #click on phpMyAdmin »
    Click Element    partial link=phpMyAdmin
    #switch to 2nd tab using title
    Switch Window   phpMyAdmin
    Input Text    id=input_username    admin
    #enter password as welcome123
    Input Text    id=input_password    welcome123
    #click on login
    Click Element    id=input_go
    #assert the error message contains "Access denied for user"
    Element Should Contain    id=pma_errors    Access denied for user
    Page Should Contain    Access denied for user
    Close Window
    #switch to 1st tab using title
    Switch Window   db4free.net - MySQL Database for free
    ${current_url}  Get Location
    Log To Console    ${current_url}

TC2 Switch Tab to Title
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.db4free.net/
    #click on phpMyAdmin »
    Click Element    partial link=phpMyAdmin
    #switch to 2nd tab
    Switch Window   NEW
    Input Text    id=input_username    admin
    #enter password as welcome123
    Input Text    id=input_password    welcome123
    #click on login
    Click Element    id=input_go
    #assert the error message contains "Access denied for user"
    Element Should Contain    id=pma_errors    Access denied for user
    Page Should Contain    Access denied for user
    Close Window
    #switch to 1st tab
    Switch Window   MAIN
    ${current_url}  Get Location
    Log To Console    ${current_url}

#1.  Navigate onto https://www.online.citibank.co.in/
#2.  Close if any pop up comes
#3.  Click on Login
#4.  Click on Forgot User ID?
#5.  Choose Credit Card
#6.  Enter credit card number as 4545 5656 8887 9998
#7.  Enter cvv number
#8.  Enter date as “14/04/2022”
#9.  Click on Proceed
#10.  Get the text and print it “Please accept Terms and Conditions”

TC3 Popup
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.online.citibank.co.in/
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose']
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose2']
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose3']

TC4 Popup Count
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.online.citibank.co.in/
    ${ele_count}    Get Element Count    xpath=//a[@class='newclose']
    #click when count > 0
    IF    ${ele_count}>0
         Click Element    xpath=//a[@class='newclose']
    END
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose2']
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose3']

TC5
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.online.citibank.co.in/
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose']
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose2']
    Click Element    xpath=//span[text()='Login']
    Switch Window   NEW
    Click Element    xpath=//div[contains(text(),'Forgot User')]
    Click Element    link=select your product type
    Click Element    link=Credit Card
    Input Text    css=#citiCard1    4454
    Input Text    css=#citiCard2    4454
    Input Text    css=#citiCard3    4454
    Input Text    css=#citiCard4    4454
    Input Text    css=input[name='CCVNO']    445
#8.  Enter date as “14/04/2000”
    #approach 1
#    Input Text    id=bill-date-long    14/04/2000
#approach 2
    Click Element    id=bill-date-long
    Select From List By Label    //select[@data-handler='selectYear']   2000
    Select From List By Label    //select[@data-handler='selectMonth']   Dec
    Click Element    link=14


    #approach 3
    Execute Javascript  document.querySelector('#bill-date-long').value='14/04/1998'
    Click Element    css=[value='PROCEED']
    Element Should Contain    xpath=//li[contains(text(),'accept Terms')]
    ...  Please accept Terms and Conditions