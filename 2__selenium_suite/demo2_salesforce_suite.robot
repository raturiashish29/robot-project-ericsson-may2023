*** Settings ***
Documentation      This suite file handles all test cases for Sign up
Library     SeleniumLibrary

*** Test Cases ***
TC1 SignUp
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://www.salesforce.com/in/form/signup/freetrial-sales/
    Input Text    name=UserFirstName    John
    Input Text    xpath=//input[contains(@id,'UserLast')]    wick
    Input Text    name=UserEmail    wick@gmail.com
    Select From List By Label    name=UserTitle     IT Manager
    Select From List By Value    name=CompanyEmployees  150
    Input Text    name=CompanyName    CCCS
    Select From List By Label    name=CompanyCountry    United Kingdom
    Click Element    xpath=//div[@class='checkbox-ui']
    Click Element    xpath=//button[@name='start my free trial']
    #below validate exact text
    Element Text Should Be    xpath=//span[contains(@id,'UserPhone')]    Enter a valid phone number
    #below validates part of the text
    Element Should Contain    xpath=//span[contains(@id,'UserPhone')]    valid phone number