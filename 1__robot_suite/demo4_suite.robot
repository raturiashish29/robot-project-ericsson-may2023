*** Settings ***
Library     OperatingSystem
Library    Collections

*** Test Cases ***
TC1
#    Create Directory    path=C:\Automation Concepts\hellofolder
    Log To Console    C:\\Automation Concepts\\hellofolder
    Log To Console    C:${/}Automation Concepts${/}hellofolder

TC2
    Create Directory    path=C:${/}Automation Concepts${/}hellofolder
    Directory Should Exist    path=C:${/}Automation Concepts${/}hellofolder
    Remove Directory    path=C:${/}Automation Concepts${/}hellofolder
    Directory Should Not Exist    path=C:${/}Automation Concepts${/}hellofolder

TC3
    #list all files in the directory C:\Program Files\WinRAR
    @{files}     List Files In Directory    path=C:${/}Program Files${/}WinRAR
    Log To Console    ${files}
    Log List    ${files}
    ${size}  Get Length    ${files}
    Log    ${size}
    Log    ${files}[0]

TC4
    Log To Console    ${CURDIR}
    Log To Console    ${OUTPUT_DIR}
    Log To Console    ${EXECDIR}
    Log To Console    Environment${SPACE}${SPACE}variable
    Log To Console    ${TEMPDIR}
    Log To Console    ${SUITE_NAME}
    Log To Console    ${TEST_NAME}

    