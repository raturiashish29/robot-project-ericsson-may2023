*** Settings ***
Library    Collections

*** Variables ***
${BROWSER_NAME}     chrome
@{COLORS}       red     green   yellow
&{MY_DETAIL}    name=balaji     mobile=77878    role=trainer
${OUTPUT_DIR}   C:${/}Automation Concepts${/}hellofolder

*** Test Cases ***
TC1
    Log To Console    ${BROWSER_NAME}

TC2
    Log To Console    ${BROWSER_NAME}
    Log To Console    ${COLORS}
    Log To Console    ${COLORS}[1]
    ${size}     Get Length    ${COLORS}
    Log To Console    ${size}

TC3
    @{fruits}   Create List     Mango   apple   orange
    #print the size of the list
    # append grapes to the list
    Append To List      ${fruits}   grapes
    # remove apple from the list
    Remove Values From List    ${fruits}    apple
    #insert jackfruit at index 0
    Insert Into List    ${fruits}    0    jackfruit
    # print the list
    Log To Console    ${fruits}
    #create a gitlab account
    #will start after lunch break at 2 PM IST

TC5
    Log To Console    ${MY_DETAIL}
    Log To Console    ${MY_DETAIL}[role]
    
    
TC6
   &{dic}   Create Dictionary   header=application/json     body=pet
   Log To Console    ${dic}

TC7
   &{opt}   Create Dictionary   platformVersion=10.1    app=zomato
   &{dic}   Create Dictionary   header=application/json     body=pet    option=${opt}
   Log To Console    ${dic}
   Log    ${dic}[option][platformVersion]
   